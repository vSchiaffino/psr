
#include <iostream>
#include <math.h>
#include<stdio.h>
#include <cmath>
#include <cstring>
#include "taller.h"
using namespace std;
/* IMPORTANTE: <math.h> y <cmath> se incluyen
 * solo para que utilicen abs() y pow(),
 * si las neceistaran. */


// Ejercicio 0
// Dados enteros a y b, decide si 'a' divide a 'b'.
bool divide(int a, int b)
{
    return b % a == 0;
}

// Ejercicio 1
// Dados dos enteros a y b, devuelve el mayor.
 int mayor(int a, int b)
{
    if(a > b) return a;
    else return b;
}

// Ejercicio 2
// Dado un n, devuelve el factorial de n.
unsigned int factorialPorCopia(int n)
{
    int resultado = 1;
    int numero_actual = 0;
    while(numero_actual < n){
        if (numero_actual != 0) resultado = resultado * (numero_actual + 1);
        numero_actual++;
    }
    return resultado;
}

// Ejercicio 3
// Dado un n por teclado, imprimir el factorial de n por pantalla.
// Hint: Usar la función anterior
void factorialPorTeclado()
{
    int n = 0;
    cout << "Ingrese la factorial que desea obtener: ";
    cin >> n;
    cout << endl << "La factorial de " << n << " es: " << factorialPorCopia(n) << endl;
}

//Ejercicio 4
// Dado un n por teclado, devuele true si y solo sí n es primo
bool esPrimo(int n)
{
    int numeroMasGrandePosible = round(sqrt(n));
    // cout << "sqrt: " << numeroMasGrandePosible;
    int i = 2;
    while(i <= numeroMasGrandePosible){
        if(n != i && n % i == 0) return false;
        i++;
    }
    return true;
}

// Ejercicio 5
// Dado un n, devolver los primos gemelos menores o iguales a n.
// Dos numeros son primos gemelos si son primos y están a distancia 2.
// Q y P son primos gemelos si P=Q+2, por ejemlo 3 y 5, 5 y 7, 11 y 13...

bool primoGemelo(int n)
{  
    if(esPrimo(n) && (esPrimo(n+2) || esPrimo(n-2))) return true;
    return false;
}


// Ejercicio 6
// Dado un numero capturado por teclado, decidir si es un palindromo.
// Por ejemplo: la entrada "hola mundo", deberia dar por resultado:
// "Numero: NO, Palindromo: NO". Mientras que "severlasalreves" deberia dar 
// "Numero: NO, Palindromo: SI"
// Hint: Ver las funciones at del tipo string y isdigit.

void esCapicua()
{
    string s = "";
    cout << "Ingrese los dígitos a comprobar: ";
    cin >> s;
    // me fijo si es un numero
    string es_n = "SI";
    for(int i = s.length() - 1; i >= 0 ; i--){
        if(!isdigit(s[i])){
            es_n = "NO";
            break;
        }
    }
    string s_al_reves = "";
    // Doy vuelta el string
    for(int i = s.length() - 1; i >= 0 ; i--){
        s_al_reves = s_al_reves + s[i];
    }
    // me fijo si es capicua
    string es_cap = "NO";
    if(s_al_reves == s) es_cap = "SI";
    cout << "El digito " << es_n << " es numérico y " << es_cap << " es capicúa." << endl;
}

// Ejercicio 7
// Dado un n, voy a escribir la secuencia de numeros de 0 a n
// Por ejemplo, n=4, da 0,1,2,3,4

void numerosHasta(int n)
{
    for(int i = 0;i <= n;i++) cout << i << " ";
    cout << endl;
}

// Ejercicio 8
// Dado un n, escribir por pantalla una escalera desde 0 hasta n.
// Ejemplo: n=3, deberia mostrar:
// 0
// 0 1
// 0 1 2
// 0 1 2 3
// Hint: Usar la función anterior

void escaleraSimple(int n)
{
    for(int i = 0;i <= n;i++) numerosHasta(i);   
}



// Ejercicio 9
// Dado un 'n', dice si es numero perfecto o no
// Los numeros perfectos son enteros tales que su valor
// es igual a la suma de sus divisores
// Por ejemplo, 6 = 3+2+1... 28=1+2+4+7+14, etc...

bool esPerfecto(int n)
{
    int acumulado = 0;
    // busco divisores
    // en el maximo para probar puse la mitad del numero mas uno porque a veces
    // redondeaba para abajo, por ejemplo en el caso del 1.Y puse la mitad para
    // que no tenga q buscar tantos numeros ya que seria ineficiente.
    for(int i = 1;i <= round(n / 2) + 1;i++){
        if(n % i == 0) acumulado += i;
    }
    if (acumulado == 0) return false;
    return acumulado == n;
}
// Ejercicio 10 (opcional)
// Dado un n capturado por teclado, imprimir el triangulo de pascal de grado n
// Por ejemplo, para n=5, deberia dar
// 1
// 1 1
// 1 2 1
// 1 3 3 1
// 1 4 6 4 1
void pascal()
{
    int numDado = 0;
    cout << "Ingrese el numero de filas que quiere tener en el triangulo: ";
    cin >> numDado;
    cout << endl;
    numDado -= 1;
    for(int n = 0;n <= numDado;n++){
        for(int k = 0; k<=n; k++){
            // uso una formula q busque
            // por si las dudas la formula es  n! / k!(n - k)!
            // donde n es la posicion horizontal y k la posicion vertical
            cout << factorialPorCopia(n) / (factorialPorCopia(k) * factorialPorCopia(n-k)) << " ";
        }
        cout << endl;
    }
    cout << endl;
}


// Ejercicio 11 (opcional)
// Dado un k positivo, devolver el k-esimo numero de la susesion de fibonacci.
long fibonacci(int k)
{
    int numeroActual = 0, aux = 0, numeroAnterior = 0;
    for (int i = 0; i <= k; i++)
    {
        if(i == 0) numeroActual, numeroAnterior = 1, 0;
        else
        {
            aux = numeroActual;
            numeroActual += numeroAnterior;
            numeroAnterior = aux;
        }
    }
    return numeroActual;
}

// Ejercicio 12 (opcional)
// Dado un n PAR capturado por teclado, intenta ver si la Conjetura de Golbach es valida para ese n.
// Golbach dijo que todo entero n PAR estrictamente mayor a dos es expresable como
// la suma de dos numeros primos(se puede repetir el primo).
// Por ejemplo, 10=3+7, 1984=107+1877, etc...

void golbach()
{
    int n = 0;
    cout << "Ingresa tu numero a analizar: ";
    cin >> n;
    cout << endl;
    // es re ineficiente lo q estoy haciendo pero creo q sirve
    for(int i = 1; i < n;i++){
        // veo si el i es primo y sino salto al prox
        if (esPrimo(i)){
            // si es primo busco otro primo de la misma forma y intento sumarlos a ver si funca
            for(int ii = i; ii < n;ii++){
            // veo si el i es primo y sino salto al prox
                if (n == i + ii && esPrimo(ii)){
                    cout << "LO ENCONTRE ES " << i << " + " << ii << " = " << n << endl;
                }
            }
        }
    }

}
